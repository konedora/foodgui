import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    int sum=0;
    String currenttext;

    int price;

    void order(String food,int value){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Comfirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            //textbox.setText("Order for "+food+" received.");
            price=value;
            sum+=value;
            totalyen.setText("Total "+sum+" yen");
            currenttext=textbox.getText();
            textbox.setText(currenttext+food+" "+value+" yen\n");
            JOptionPane.showMessageDialog(null, "Order for "+food+" received.");

        }
    }

    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea textbox;
    private JLabel totalyen;
    private JButton takoyakiButton;
    private JButton katsudonButton;
    private JButton gyozaButton;
    private JButton checkoutButton;
    private JButton cancelButton;

    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",700);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",800);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",600);
            }
        });

        takoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Takoyaki",750);
            }
        });
        katsudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Katsudon",650);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",500);
            }
        });

        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Comfirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    if(sum!=0) {

                        JOptionPane.showMessageDialog(null, "Thank you! The total price is " + sum + " yen.");
                        textbox.setText("");
                        sum = 0;
                        totalyen.setText("Total " + sum + " yen");
                        price=0;
                        currenttext="";
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "You have not ordered.");
                    }


                }
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel?",
                        "Cancel Comfirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    textbox.setText(currenttext);
                    sum-=price;
                    totalyen.setText("Total " + sum + " yen");
                    price=0;
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
